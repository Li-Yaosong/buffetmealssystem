isEmpty(BUILD_TREE) {
    sub_dir = $$_PRO_FILE_PWD_
    sub_dir ~= s,^$$re_escape($$clean_path($$PWD/../)),,
    BUILD_TREE = $$clean_path($$OUT_PWD)
    BUILD_TREE ~= s,$$re_escape($$sub_dir)$,,
}

!isEmpty(CONAN_INSTALL) {
    include(conandeploy.pri)
}

!isEmpty(LIB_NAME) {
    include(conanupload.pri)
}
